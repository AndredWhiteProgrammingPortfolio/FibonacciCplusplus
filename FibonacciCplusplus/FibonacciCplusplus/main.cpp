//
//  main.cpp
//  FibonacciCPlusPlus
//
//  Created by Andre White on 5/10/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#include <iostream>
using namespace std;
//fibonacci algorithm
int fib(int n){
    int t[3]; //array to hold F(n-2) F(n-1) and F(n)
    t[0]=0;   //base case for F(0)
    t[1]=1;   //base case for F(1)
    //handle base case.
    if (n<2) {
        return t[n];
    }
    else{
        for (int x=2; x<n+1; x++) {
            t[2]=t[0]+t[1];
            t[0]=t[1];
            t[1]=t[2];
        }
    }
        return t[2];
}
void prompt(){
    int n;
    cout<<"Enter positive integer n to calculate nth Fibonacci Number"<<endl;
    cin>>n;
    //make sure the number is positive
    while(n<0) {
        cout<<"Enter a POSITIVE integer n"<<endl;
        cin>>n;
    }
    //display fibonnaci number
    cout<<"Fibonnaci number for "<<n<<" is "<<fib(n)<<endl;
}

int main(int argc, const char * argv[]) {
    char answer='Y';
    cout<<"Fibonnaci Number with Dynamic Programming"<<endl;
    while (toupper(answer)=='Y') {
        prompt();
        cout<<"Try again?(Y/N)";
        cin>>answer;
        //make sure answer is y or n
        while (toupper(answer)!='Y'&&toupper(answer)!='N') {
            cout<<"Try again?(Y/N)";
            cin>>answer;
        }
    }
    
    return 0;
}
