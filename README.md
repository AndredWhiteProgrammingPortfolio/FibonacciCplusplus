<snippet>
   <content>
# Fibonnaci Dynamic Programming algorithm with C++
This is a solution to the Fibonnaci problem with a dynamic programming algorithm. This algorithm is a more efficient algorithm than the more common recursive fibonnaci solution with a time complexity of O(n) and has a better space complexity than a standard O(n) dynamic algorithm, as it only uses an array of size 3.
## Installation
To intall, you can clone the repository to build and run in XCode or simply the cpp file for compilation with your own compiler. 
## License
MIT
</content>
</snippet>
